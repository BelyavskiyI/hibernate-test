package com.belyavskiy;


import com.belyavskiy.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.SQLException;
import java.time.LocalDate;

public class HibernateRunner {
    public static void main(String[] args) throws SQLException {

        Configuration configuration = new Configuration();
//        configuration.addAnnotatedClass(User.class);
        configuration.configure();
        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            session.beginTransaction();

            User user = User.builder()
                    .username("igor@gmail.com")
                    .firstname("igor")
                    .lastname("belyavskiy")
                    .birthDate(LocalDate.of(1986, 1, 19))
                    .age(36)
                    .build();
            session.save(user);
            session.getTransaction().commit();
        }

    }
}